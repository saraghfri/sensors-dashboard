import request from '@/request'

export function getSensors() {
    return request({
        url: '/sensors',
        method: 'get',
    })
}

export function getSensorData(sensorId) {
    return request({
        url: `/sensorData?sensorId=${sensorId}`,
        method: 'get',
    })
}