import axios from 'axios'

const api = axios.create({
    baseURL: process.env.VUE_APP_BASE_API,
    withCredentials: false,
    headers: {
        'Accept': 'application/json'
    }
})

// response interceptor
api.interceptors.response.use(undefined,
    error => {
        // if (error.response.status === 401) {
        // localStorage.clear()
        // }
        return Promise.reject(error)
    }
)
api.interceptors.request.use(
    config => {
        // let token = ''
        // if (JSON.parse(localStorage.getItem('userData'))) {
        //     token = JSON.parse(localStorage.getItem('userData')).token
        // }
        // if (token) {
        //     config.headers.Authorization = `Bearer ${token}`
        // }

        return config
    },
    error => Promise.reject(error)
)
export default api;