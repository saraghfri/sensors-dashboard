import {defineStore} from "pinia";

export const useGlobalStore = defineStore('global', {
    state: () => {
        return {
            displayToast: false,
            response_data: null,
            response_type: null,
            full_loading: false,
        }
    },
    actions: {
        toastTimeOut() {
            this.displayToast = true

            setTimeout(() => {
                this.displayToast = false
            }, 3000);
        },
        setDisplayToast(value) {
            this.displayToast = value
        },

        setResponseData(value) {
            this.response_data = value
        },
        setResponseType(value) {
            this.response_type = value
        },
        setFullLoading(value) {
            this.full_loading = value
        },
    },
    getters: {
        getResponseData() {
            return this.response_data
        },
        getResponseType() {
            return this.response_type
        },
        getFullLoading() {
            return this.full_loading
        },
    },


})
