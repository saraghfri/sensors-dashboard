import {createRouter, createWebHistory} from 'vue-router';
import Home from '@/views/index.vue';
import About from '@/views/about/index.vue';

const routes = [
    {
        path: '/',
        component: Home,
        meta: {
            title: 'Sensors List',
        },
    },
    {
        path: '/about',
        component: About,
        meta: {
            title: 'About Us',
        },
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

router.beforeEach((to, from, next) => {
    document.title = to.meta.title || 'Sensor Dashboard';
    next();
});

export default router;
